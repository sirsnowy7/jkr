class Joke
  # one liner to get a new joke:
  # new_joke = Joke.new(Joke.get_joke)
  
  attr_accessor :id, :type, :setup, :punchline
  def initialize(cont)
    cont = JSON.parse(cont.force_encoding("ISO-8859-1"))
    @id = cont["id"]
    @type = cont["type"]
    @setup = cont["setup"]
    @punchline = cont["punchline"]
  end
  
  def self.get_joke(addr="https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_joke")
    begin
      joke = open(addr)
      IO.copy_stream(joke, "joke.json")
    rescue
      return 1
    end
    file = File.open("joke.json", "r")
    cont = file.read
    return cont
  end
end
