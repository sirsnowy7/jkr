class Jkr
  def self.main
    ex = false
    print "Press enter to get a joke. Type q to quit.\n"
    loop do
      case gets.chomp
      when "q"
        exit(0)
      when "e"
        ex = true
      else
        new_joke = Joke.new(Joke.get_joke)
        if new_joke == 1
          puts "I didn't get the joke. Try again later."
        end
        if ex
          puts new_joke.id
          puts new_joke.type
        end
        print new_joke.setup
        gets.chomp
        puts new_joke.punchline
      end
    end
  end
end
